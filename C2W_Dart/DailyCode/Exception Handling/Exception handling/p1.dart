import "dart:io";

void main(){
  print("Start main");
  print("Enter value");
  try {
    int? data=int.parse(stdin.readLineSync()!);
    print(data);
  }catch(ex){
    print("Exception Handled");
    print(ex);
  }
  print("End Main");
}
/*
Start main
Enter value
10
10
End Main
-----------------------
Start main
Enter value
mohit
Exception Handled
FormatException: Invalid radix-10 number (at character 1)
mohit
^

End Main

*/