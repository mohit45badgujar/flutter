// WAP to print Square of odd digit and Cube of even digit btn 40 to 50

 void main(){
  int y=40;
  while(y<=50){
    if(y%2!=0){
      print(y*y);
    }else{
      print(y*y*y);
    }
    y++;
  }
}