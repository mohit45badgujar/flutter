// static Variables

class Demo {
  int x=10;
  static int y=20;

  void printData(){
    print(x);
    print(y);

  }
}
void main(){
  Demo obj1=new Demo();
  obj1.printData(); 
                    
  Demo obj2=Demo(); 
  obj2.printData();

  print("==============================");

 // obj1.y=50;  Error: The setter 'y' isn't defined for the class 'Demo'.
  Demo.y=50;
  obj1.printData();
  obj2.printData();                
}
/*
10
20
10
20
==============================
10
50
10
50
*/