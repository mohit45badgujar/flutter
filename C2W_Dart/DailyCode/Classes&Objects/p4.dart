import 'dart:io';

class train {
  String? tName="Maharashtra";
  int? noOfStation=15;
  double? tSpeed=80.50;

  void trainInfo(){
    print("Train Name: $tName");
    print("Numb of Stations: $noOfStation");
    print("Train Speed: $tSpeed");
  }
}
void main(){
  train obj=new train();
  obj.trainInfo();

  print("Enter Train Name:");
  obj.tName=stdin.readLineSync();

  print("Enter Numb of Stations:");
  obj.noOfStation=int.parse(stdin.readLineSync()!);

  print("Train Speed:");
  obj.tSpeed=double.parse(stdin.readLineSync()!);
    
  obj.trainInfo();
}
/*
Train Name: Maharashtra
Numb of Stations: 15
Train Speed: 80.5
Enter Train Name:
Pune Hamsafar
Enter Numb of Stations:
20
Train Speed:
85.20
Train Name: Pune Hamsafar
Numb of Stations: 20
Train Speed: 85.2
*/