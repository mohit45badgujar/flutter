class Parent {
  int x=10;
  String str="surname";

  void parentDisplay(){
    print(x);
    print(str);
  }
}
class Child extends Parent {
  int y=20;
  String str2="name";

  void childDisp(){
    print(y);
    print(str2);
  } 
}

void main(){
  Parent obj1=new Parent();
  print(obj1.x);
  print(obj1.str);
  obj1.parentDisplay();

  Child obj2=new Child();
  print(obj2.x);
  print(obj2.str);
  obj2.parentDisplay();

  print(obj2.y);
  print(obj2.str2);
  obj2.childDisp();

}
/*
10
surname
10
surname
surname
20
name
20
name
*/