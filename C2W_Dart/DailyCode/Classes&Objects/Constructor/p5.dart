// Way to write Constructor

class Demo {
  int x=10;
  static int y=20;

}
void main(){
  //print(x); // Error: Undefined name 'x'
  //print(y); //  Error: Undefined name 'y'.

  Demo obj=new Demo();
  print(obj.x);
  //print(obj.y); //  Error: The getter 'y' isn't defined for the class 'Demo'.
}