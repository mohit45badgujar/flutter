class test{
  int? x;
  int? y;
  int? z;
  static int w=199;
  test(this.x,this.y,this.z);

  get getX=>x;
  
  get getY{
    return y;
  }
  get getZ{
    return z;
  }
  static get getW{
    return w;
  }
}
void main(){
  test obj=new test(10,20,30);
  print(obj.getX);
  print(obj.getY);
  print(obj.getZ);
  print(test.getW);
}