class Demo {
  Demo._private(){
    print("private Constructor");
  }
  factory Demo(){
    print("factory Constructor");
    return Demo._private();
  }
}