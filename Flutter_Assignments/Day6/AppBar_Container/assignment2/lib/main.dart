import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment2(),
    );
  }
}

class Assignment2 extends StatefulWidget{
  const Assignment2({super.key});
  
  @override
  State<Assignment2>createState()=>_AssignmentState();
}
class _AssignmentState extends State<Assignment2> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text(
          "Assignment 2",
        ),
        centerTitle: true,
          
        
        actions: const[
          Icon(
            Icons.search_outlined,
          ),
          Icon(
            Icons.more_vert,
          )
        ],
      ),
    );
  }

}
