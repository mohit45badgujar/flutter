import 'package:flutter/material.dart';
//import 'package:gradient_app_bar/gradient_app.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizApp(),
      
    );
  }
}
class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override

  State createState()=> _QuizAppState();
}

class SingleModelQuestion {
  final String? question;
  final List<String>? options;
  final int? correct;
  final String? image;

   const SingleModelQuestion({this.question,this.options,this.correct,this.image});
}

class _QuizAppState extends State {

  List allQuestions=[
     const SingleModelQuestion(
      question: "Which cricketer holds the record for the most sixes in the ICC Cricket WC-2023?",
      options: ["Rohit Sharma", "David Warner", "Glenn Maxwell", "Shreyas Iyer"],
      correct:0,
      image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGrWiLy3gd6ybWQdm828ycK4xPUN8gSc7BcBPHaqJRgr55ElDvngiDoiXFM_89p2E62zU&usqp=CAU",
  
    ),
    const SingleModelQuestion(
      question: " Who was the highest run scorer in WC-2023?",
      options: ["Rohit Sharma", "Virat Kohli", "Quinton de Kock", "Rachin Ravindra"],
      correct:1,
      image:"https://i.pinimg.com/236x/6c/c5/43/6cc543c7501f95370c6dc5bbbe963ba2.jpg",
    ),
    const SingleModelQuestion(
      question: " Who tooks the highest wickets in WC-2023?",
      options: ["Adam Zampa", "Dilshan Madushanka", "Mohammed Shami", "Jasprit Bhumrah"],
      correct:2,
      image:"https://akm-img-a-in.tosshub.com/sites/visualstory/wp/2023/09/Mohammed-Shami-4.jpg?size=*:900",
      
    ),
    const SingleModelQuestion(
      question: " Which cricketer holds the record for the most fours in the ICC Cricket WC-2023?",
      options: ["Rachin Ravindra", "Rohit Sharma", "Quinton de Kock", "Glenn Maxwell"],
      correct:1,
      image:"https://img1.hscicdn.com/image/upload/f_auto,t_ds_w_1200/lsci/db/PICTURES/CMS/368700/368779.jpg",
    ),
    const SingleModelQuestion(
      question: " Which Two Teams Played in the ICC cricket WC Final of 2023?",
      options: ["IND and SA", "IND and PAK", "IND and NZ", "IND and AUS"],
      correct:3,
      image:"https://images.thequint.com/thequint%2F2023-11%2F88ad9e21-dbe2-492f-a72d-1161e38513c9%2FIndia_vs_Australia_World_Cup_Final_2023.jpg?auto=format%2Ccompress&fmt=webp&width=120&w=1200",
    ),

  ];
  
  bool questionScreen=true;
  int queNo=0;
  int selected=-1;
  int result=0;
  String msg="";


  MaterialStateProperty<Color>giveColor(int index){
    if(selected !=-1){
      if(index==allQuestions[queNo].correct){
        return const MaterialStatePropertyAll(Colors.green);
      }else if(index == selected){
        return const MaterialStatePropertyAll(Colors.red);
      }else{
        return const MaterialStatePropertyAll(Colors.blue);
      }
    }
    return const MaterialStatePropertyAll(Colors.blue);
  }

  void validatePage(){
    if(selected == -1){
      msg="Please Select an option";
    }else{
      if(selected==allQuestions[queNo].correct){
        result++;
      }
      if(queNo==allQuestions.length-1){
        questionScreen=false;
        msg="";
        selected=-1;
      }else{
        msg="";
        queNo++;
        selected=-1;
      }
    }setState(() {
      
    });
  }

  SizedBox printResult(){
    if(result ==0){
      return SizedBox(
        width:300,
        child: Column(
          children: [
            const Text("Opps...",
            style: TextStyle(fontSize:30,
            fontStyle: FontStyle.italic),
            ),
            Text("You Have Scored $result out of ${allQuestions.length}",
            style: const TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.italic
            ),
            ),
            const Text("Better Luck Next Time...",
            style: TextStyle(fontSize: 20,
            fontStyle: FontStyle.italic
            ),
            ),
            
        ]),
      

      );
    }else{
      return SizedBox(
        width:300,
        child: Column(
          children: [
            Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0rBsokVE883q6YvKdytzkwHNNVzM_gqMqTg&usqp=CAU",
            height: 300,
            width: 300,
            ),
            const Text(
              "Congratulations !!!",
              style: TextStyle(fontSize: 30,
              fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 10),
            Text(
              "You Have Scored $result out of ${allQuestions.length}",
              style: const TextStyle(fontSize: 20,
              fontStyle: FontStyle.italic),
              ),
          ]),
      );
    }
  }

  Scaffold isQuestionScreen(){
    if(questionScreen){
      return Scaffold(
        
        appBar:AppBar(
          
          backgroundColor: Colors.blue,

          title:const Text("Quiz App",
          textDirection: TextDirection.ltr,
          style: TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.w600,
            //color: Color.fromARGB(221, 33, 27, 27),
          )),
          shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(30)
          )
        ),
          centerTitle: true,
         
         ),

         floatingActionButton: FloatingActionButton(
          onPressed:() {
            validatePage();
          },
          backgroundColor: const Color.fromARGB(255, 204, 235, 249),
          child: const Icon(
            Icons.forward,
            color: Colors.orange,
          ),
         ),


         body:
         SingleChildScrollView(
           child: Container(
            height :MediaQuery.of(context).size.height,
            width: double.infinity,
            color: Colors.black,
         
          /*  
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                //stops: [0.1,0.4,0.6,0.9],
                
                colors:[
               //   Color.fromARGB(255, 16, 21, 164),
               //  Color.fromARGB(255, 42, 21, 147),
              //   Color.fromARGB(255, 48, 2, 113),
                  Colors.black,
                  Colors.red,
                  //Colors.indigo,
                  //Colors.teal,
                ],
                
              ),
            ),*/
            child:
            Column(
              children: [
              const SizedBox(
                height: 30,
              ),
           
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                   Text(
                    "Questions :${queNo+1}/${allQuestions.length}",
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                      color: Colors.white
                    ),
                  ),               
                ],
              ),
              const SizedBox(
                height: 20,
              ),
           
              SizedBox(
                width: 320,
                //height: 50,
                child: Text(
                  allQuestions[queNo].question,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w400,
                    color: Colors.white70,
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
           
              SizedBox(
                height: 40,
                width: 300,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(0),
                  ),
                  onPressed:(){
                  
                    if(selected==-1){
                      selected=0;
                    }
                    setState((){}
                    );    
                  },
           
                
                  child: Text(
                    "A. ${allQuestions[queNo].options[0]}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(6, 1, 1, 0.867),
                    )),
                  ),
              ),
              
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 40,
                width: 300,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(1),
                  ),
                  onPressed:(){
                  
                    if(selected==-1){
                      selected=1;
                    }
                    setState((){}
                    );    
                  },
                  child: Text(
                    "B. ${allQuestions[queNo].options[1]}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87,
                    )),
                  ),
              ),
                  
               const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 40,
                width: 300,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(2),
                  ),
                  onPressed:(){
                  
                    if(selected==-1){
                      selected=2;
                    }
                    setState((){}
                    );    
                  },
                  child: Text(
                    "C.${allQuestions[queNo].options[2]}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87,
                    )),
                  ),
              ),   
              
              const SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 40,
                width: 300,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: giveColor(3),
                  ),
                  onPressed:(){
                  
                    if(selected==-1){
                      selected=3;
                    }
                    setState((){}
                    );    
                  },
                  child: Text(
                    "D.${allQuestions[queNo].options[3]}",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87,
                    )),
                  ),
              ),
              
              const SizedBox(height: 15,),
              Text(msg,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal
              )),
              //const Image(image:NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGrWiLy3gd6ybWQdm828ycK4xPUN8gSc7BcBPHaqJRgr55ElDvngiDoiXFM_89p2E62zU&usqp=CAU")),
              (selected!=-1)?Image.network(allQuestions[queNo].image,height: 250,):Container(),
            ],
           ),
           ),
         ));
     
    }else{
      return Scaffold(
        //backgroundColor: const Color.fromARGB(31, 204, 64, 13),
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text("Quiz App",
          style: TextStyle(
            fontSize: 30,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.w600,
          )),
          centerTitle: true,
        ),

        body: Container(
          height: double.infinity,
          width: double.infinity,
          
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              //stops: [0.1,0.4,0.6,0.9],
              
              colors:[
             //   Color.fromARGB(255, 16, 21, 164),
             //  Color.fromARGB(255, 42, 21, 147),
            //   Color.fromARGB(255, 48, 2, 113),
                Colors.yellow,
                Colors.red,
                //Colors.indigo,
                //Colors.teal,
              ],
              
            ),
          ),child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children:[
                printResult(),

                const SizedBox(
                  height: 30,
                ),

                ElevatedButton(onPressed:(){
                  setState(() {
                    
                      questionScreen=true;
                      queNo=0;
                      result=0;
                    });
                  },
            
                  style:const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.blue)
                  ),
                  child:const Text("Reset")
                  ),     
              ],
            )
          ],
        )));
    }
  }
              
  @override
  Widget build(BuildContext context){
    return isQuestionScreen();
  }
}