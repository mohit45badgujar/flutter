

import 'dart:io';
void main(){
  int? empId;
  String? empName;
  double? empSalary;

  print("Enter employee Id");
  empId=int.parse(stdin.readLineSync()!);
  
  print("Enter Employee Name");
  empName=stdin.readLineSync();

  print("Enter Employee Salary");
  empSalary=double.parse(stdin.readLineSync()!);

  stdout.write("Id=$empId , Name=$empName , Salary=$empSalary \n");
}
/*
Enter employee Id
2
Enter Employee Name
Mohit
Enter Employee Salary
9.5
Id=2 , Name=Mohit , Salary=9.5 
*/