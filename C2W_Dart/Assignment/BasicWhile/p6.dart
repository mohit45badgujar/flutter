// WADP to print sum of all even numbers and the multiply of the odd numbers between 1 to 10.

void main(){
  int x=1;
  int sum=0;
  int mul=1;
  while(x<=10){
    if(x%2 == 0){
      sum=sum+x;
    } 
    
    if(x%2!=0){
      mul=mul*x;
    }
    x++;
  }
  print(sum);
  print(mul);
}