class Parent{
  Parent(){
    print("In Parent Constr");
  }
}
class Child extends Parent {
  Child(){
    super();
    print("In Child Constr");
  }
}
void main(){
  Child obj=new Child();
}