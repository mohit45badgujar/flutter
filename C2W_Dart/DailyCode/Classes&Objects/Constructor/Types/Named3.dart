class Employee{
  int? empId=10;
  String? empName="Krishna";

  Employee(){
    print("Default");
  }
  Employee.constr(int empId,String empName){
    print("Parameterized");
  }
}
void main(){
  Employee obj1=new Employee();
  Employee obj2=new Employee.constr(10,"Krishna");
}
/*Output:Default
      Parameterized
      */