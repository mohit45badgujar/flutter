/*  

10 20 30 40 50
60 70 80 90 100
101 102 103 104 105 
106 107 108 109 110
120 130 140 150 160
*/
import 'dart:io';
bool isDuck(int x){
  int count=0;
  while(x != 0){
    if(x%10==0){
      count++;
    }
    x=x ~/ 10;
  }
  if(count == 0){
    return false;
  }else{
    return true;
  }

}
void main(){
  int? row=int.parse(stdin.readLineSync()!);
  int n=1;
  for(int i=1;i<=row;i++){
    for(int j=1;j<=row;){
      if(isDuck(n)){
        stdout.write("${n} ");
        j++;
      }
      n++;
    }
    print(" ");
  }
}


              