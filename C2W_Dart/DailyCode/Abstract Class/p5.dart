abstract class Demo {
  void fun1(){
    print("In fun1");
  }
  void fun2();
}
class DemoChild extends Demo {
  void fun2(){
    print("In Fun2");
  }
}
void main(){
  DemoChild obj=new DemoChild();
  obj.fun2();
}
// In Fun2