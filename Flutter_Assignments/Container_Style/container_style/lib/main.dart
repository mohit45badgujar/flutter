import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home:ContainerStyle(),
    );
  }
}

class ContainerStyle extends StatefulWidget{
  const ContainerStyle({super.key});
  @override
  State <ContainerStyle> createState()=> _ContainerStyleState();
}

class _ContainerStyleState extends State<ContainerStyle>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title: const Text("Container Styling"),
        backgroundColor: Colors.blue,
      ),
      /*body:Container(
        height: 200,
        width: 200,
        color: Colors.red,
      ),*/
      /*body:Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        child: Container(
          margin: const EdgeInsets.only(top: 10,bottom: 10,left: 10),
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
      ),*/

/*      body: Container(
        height: 300,
        width: 300,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.yellow,
            width: 5,
          ),
        ),
      ),*/

      // borderRadius :
      /*body: Container(
        height: 200,
        width: 200,
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          border: Border.all(
            color: Colors.blue,
            width: 5,
          ),
        ),
      ),*/

      //boxShadow
      /*body: Container(
        height: 200,
        width: 200,
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          border: Border.all(
            color: Colors.blue,
            width: 5,
          ),
          boxShadow: const[
            BoxShadow(
              color: Colors.purple,offset: Offset(30, 30),blurRadius: 8),
            BoxShadow(
              color: Colors.red,offset: Offset(20, 20),blurRadius: 8),
            BoxShadow(
              color: Colors.green,offset: Offset(10, 10),blurRadius: 8),           
          ]
        ),
      ),*/

      //gradient
      body: Container(
        height: 200,
        width: 200,
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: const BorderRadius.all(Radius.circular(20)),
          border: Border.all(
            color: Colors.blue,
            width: 5,
          ),
          gradient: const LinearGradient(
            stops: [0.3,0.5],
            colors: [Colors.red,Colors.green]
        ),
      ),
      )
      
    );
  }
}
