// Create an AppBar,give an Icon at the start of the appbar,give a title in the middle,and at the end add an icon

import 'package:flutter/material.dart';

class Question1 extends StatefulWidget {
  const Question1({super.key});
  @override
  State createState()=> _Question1State();

}

class _Question1State extends State {
  
  @override
  Widget build(BuildContext context){
    
    return Scaffold(
      appBar:AppBar(
        leading: const Icon(Icons.menu),
        title: const Text("IconPlace",
        ),
        centerTitle: true,
        actions: const [
          Icon(
            Icons.search,
          ),
        ],
      ) ,
    );
  }
}