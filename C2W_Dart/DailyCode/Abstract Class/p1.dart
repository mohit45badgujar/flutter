class Parent {
  void property(){
    print("Gold,Cars,Farm,Flats");
  }
  //void career();  Error: 'Parent.career'is defined here.
  //void marry();  Error: 'Parent marry'is defined here.
}
void main(){

}
/*
 Error: The non-abstract class 'Parent' is missing implementations for these members:
 - Parent.career
 - Parent.marry
Try to either
- provide an implementation,
 - inherit an implementation from a superclass or mixin,
 - mark the class as abstract, or
 - provide a 'noSuchMethod' implementation.
 */