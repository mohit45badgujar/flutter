class Parent {
  
  void career(){
    print("Engineering");
  }
  void marry(){
    print("Deepika Padukone");
  }
}
class Child extends Parent {
  void marry(){
    print("Disha Patani");
  }
  void profession(){
    print("Software Engineering");
  }
}
void main(){
  Parent obj1=new Child();
  obj1.career();
  obj1.marry();
 // obj1.profession();  Error: The method 'profession' isn't defined for the class 'Parent'.

}
