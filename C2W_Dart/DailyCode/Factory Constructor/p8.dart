/*
class Demo {
  static Demo obj1=new Demo();
  Demo Demo(){
    print("Constructor");
    return obj1;
  }
}
void main(){
  Demo obj=new Demo();
}
Error: Constructors can't have a return type.  
Try removing the return type.
    return obj1;
    ^

*/