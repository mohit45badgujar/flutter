import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:ListViewDemo(),
    );
  }
}

class ListViewDemo extends StatefulWidget{
  const ListViewDemo({super.key});
  @override
  State createState()=> _ListViewDemoState();
}

class T20Players{
  late final List? t20Playes;
}
/*
List Playes=(
const T20Players(
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",
)

const OneDayPlayers(
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",
)
const TestPlayers(
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",

)
);
*/
List playersList=[
  [
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",
  ],
  [
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",
  ],
  [
  "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_320,q_50/lsci/db/PICTURES/CMS/331100/331165.png",
  "https://resources.pulse.icc-cricket.com/players/33992/284/108.png",
  "https://latturam.com/wp-content/uploads/2023/12/Cricket-Player-Rinku-Singh-Biography-Wiki.webp",
  "https://static-files.cricket-australia.pulselive.com/headshots/440/10953-camedia.png",
  ]

];
List<String> info=["T-20 Players","One Day Players","Test Players"];


class _ListViewDemoState extends State {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("List View Demo"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ListView.separated(
        itemCount:playersList.length ,
        separatorBuilder: (BuildContext context,int index){
          return const Text("*************************************************************");
        },
        itemBuilder: (BuildContext context,int index){
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(playersList[index][0],
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),),
                Image.network(playersList[index][0]),
                Image.network(playersList[index][1]),
                Image.network(playersList[index][2]),
                Image.network(playersList[index][3]),
                Text(info[index]),
              ],
              
            ),
            
            );
        }
      ),
    );
  }
}