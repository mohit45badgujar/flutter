
class Demo {
  int x=10;
  String str="classObject";

  void info(){
      print(x); // 10
      print(str); // classObject
  }
}
void main(){
 // info();   Error: Method not found: 'info'.
 // print(x);   Error: Undefined name 'x'.
  Demo obj=new Demo();
  obj.info();

  print(obj.x); // 10
  print(obj.str); // classObject
}