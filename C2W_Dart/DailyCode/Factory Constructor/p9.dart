/*
class Demo {
  Demo(){
    print("In Const");

  }
  factory Demo(){
    print("In Factory");
    return 10;
  }
}
void main(){
  Demo obj=new Demo();
}
*/
// Error: Can't use 'Demo' because it is declared more than once.