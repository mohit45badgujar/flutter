class Parent {
  int x=10;
  String str="surname";

  void parentDisplay(){
    print("In Parent Display");
  }
}
class Child extends Parent {
  int y=20;
  String str2="name";

  void childDisp(){
    print(y);
    print(str2);
  } 
}

void main(){
  Parent obj=new Parent();
  print(obj.x);
  print(obj.str);
  obj.parentDisplay();
}
/*
10
surname
In Parent Display
*/