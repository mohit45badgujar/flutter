import 'dart:io';
void main(){
  int? empId;
  String? empName;
  double? empSalary;

  print("Enter employee Id");
  empId=int.parse(stdin.readLineSync()!);
  
  print("Enter Employee Name");
  empName=stdin.readLineSync();

  print("Enter Employee Salary");
  empSalary=double.parse(stdin.readLineSync()!);

  print("Employee Id= $empId");
  print("Employee Name = $empName");
  print("Employee Salary= $empSalary");

}
/*
Enter employee Id
2
Enter Employee Name
Mohit
Enter Employee Salary
9.5
Employee Id= 2
Employee Name = Mohit
Employee Salary= 9.5
*/