

abstract class Devloper {
  factory Devloper(String devType){
    if(devType=="BackEnd"){
      return BackEnd();
    }else if(devType=="FrontEnd"){
      return FrontEnd();
    }else if(devType=="Mobile"){
      return Mobile();
    }else{
      return Other();
    }
  }
  void devLang();
}
class BackEnd implements Devloper{
  void devLang(){
    print("NodeJS/SpringBoot");
  }
}
class FrontEnd implements Devloper{
  void devLang(){
    print("ReactJS/AngularJS");
  }
}
class Mobile implements Devloper{
  void devLang(){
    print("Flutter/Android");
  }
}
class Other implements Devloper{
  void devLang(){
    print("Testing/DevOps/Support");
  }
}

void main(){
  Devloper obj=new Devloper("FrontEnd");
  obj.devLang();
  Devloper obj1=new Devloper("BackEnd");
  obj1.devLang();
  Devloper obj2=new Devloper("Mobile");
  obj2.devLang();
  Devloper obj3=new Devloper("Testing");
  obj3.devLang();
}
/*
ReactJS/AngularJS
NodeJS/SpringBoot
Flutter/Android
Testing/DevOps/Support
*/