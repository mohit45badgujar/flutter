import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {
  int quizNo = 1;
  int result = 0;
  bool correctA = false;
  bool correctB = false;
  bool correctC = false;
  bool correctD = false;
  final List<String> questions = [
    "When and where was Rohit Sharma born?",
    "Apart from Mumbai Indians Which is the other franchise he played?",
    "Which are the national teams did Rohit Sharma played for in his career?",
    "How many national awards has Rohit won  so far?"
    "Who hold the world record of highest individual ODI score?",
    "What is the nickname of Rohit Sharma ?",
    "Who was Rohit Sharma's first Coach",
    "In which year did Rohit Sharma win the ICC ODI cricketer of the year?",
    
  ];
  final List<String> optionA = [
    "April 30,1987 at Nagpur",
    "Hydrabad Deccan Chargers in 2008",
    "Mumbai Indians",
    "3",
    "Rohit Sharma 264",
    "Yuzi",
    "Dinesh Lad",
    "2016",
  ];
  final List<String> optionB = [
    "May 30,1986 at Nagpur",
    "CSK in 2008",
    "Deccan Charges",
    "2",
    "Sachin Tendulkar 264",
    "Hitman",
    "Lalchand Rajput",
    "2017",
  ];
  final List<String> optionC = [
    "April 30,1987 at Mumbai",
    "Kings XI Punjab in 2007",
    "Mumbai in Ranji",
    "5",
    "Sehwag 264",
    "Chiku",
    "Ravi Shashri",
    "2018",
  ];
  final List<String> optionD = [
    "May 30,1986 at Mumbai",
    "Rajasthan Royals in 2007",
    "All of the Above",
    "1",
    "Virat Kohli 264",
    "Boom Boom",
    "Duncan Fletcher",
    "2019",
  ];
  final List<String> correct = ["A", "A", "D", "B", "A","B","A","D"];
  bool displayResult = false;

  ButtonStyle giveColor(istrue) {
    if (istrue) {
      return const ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 234, 161, 202)));
    } else {
      return const ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Color.fromARGB(255, 252, 144, 144)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 190, 166, 8),
          title: const Text(
            "About Rohit Sharma",
            style: TextStyle(fontStyle: FontStyle.italic),
            
          
          ),
          centerTitle: true,
          //backgroundColor: const Color.fromARGB(255, 188, 65, 250),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepPurple,
          onPressed: () {
            setState(() {
              if (quizNo < questions.length) {
                quizNo++;
              }

              correctA = false;
              correctB = false;
              correctC = false;
              correctD = false;
            });
          },
          child: const Text("Next"),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 15,
                ),
                Text("Question : $quizNo / 8",
                    style: const TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 10,
                ),
                Text("Question : ${questions[quizNo - 1]}",
                    style: const TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "A") {
                      result++;
                      setState(
                        () {
                          correctA = true;
                        },
                      );
                    }
                  },
                  style: giveColor(correctA),
                  child: Text("A . ${optionA[quizNo - 1]}"),
                ),
                //style:const ButtonStyle(shadowColor:MaterialStatePropertyAll())),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "B") {
                      result++;
                      setState(() {
                        correctB = true;
                      });
                    }
                  },
                  style: giveColor(correctB),
                  //style: (correctB) ? giveColor(correctB) : giveWrong(correctB),
                  child: Text("B . ${optionB[quizNo - 1]}"),
                ),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "C") {
                      result++;
                      setState(() {
                        correctC = true;
                      });
                    }
                  },
                  style: giveColor(correctC),
                  //style: (correctC) ? giveColor(correctC) : giveWrong(correctC),
                  child: Text("C . ${optionC[quizNo - 1]}"),
                ),
                const SizedBox(
                  height: 5,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (correct[quizNo - 1] == "D") {
                      result++;
                      setState(
                        () {
                          correctD = true;
                        },
                      );
                    }
                  },
                  style: giveColor(correctD),
                  //style: (correctD) ? giveColor(correctD) : giveWrong(correctD),
                  child: Text("D . ${optionD[quizNo - 1]}"),
                ),
                const SizedBox(height: 30),
                (quizNo == questions.length)
                    ? ElevatedButton(
                        child: const Text("Result"),
                        onPressed: () {
                          setState(() {
                            displayResult = true;
                          });
                        },
                      )
                    : Container(),
                const SizedBox(
                  height: 30,
                ),
                (displayResult)
                    ? SizedBox(
                        child: Text("$result / 5",
                            style: const TextStyle(fontSize: 20)),
                      )
                    : Container(),
              ],
            )
          ],
        ));
  }
}
