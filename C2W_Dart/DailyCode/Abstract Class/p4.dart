abstract class Parent {
  void property(){
    print("Gold,Cars,Farm,Flats");
  }
  void career();
  void marry();
}
class Child extends Parent {
  
  void career(){
    print("Youtuber");
  }
  
  void marry(){
    print("Selena");
  }
}
void main(){
  Child obj=new Child();
  obj.property();
  obj.career();
  obj.marry();
}