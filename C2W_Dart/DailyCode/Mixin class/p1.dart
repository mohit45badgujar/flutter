/*
mixin DemoParent {
  void m1(){
    print("In m1-DemoParent");
  }
}
class Demo {
  void m2(){
    print("In m2-Demo");
  }
}
class DemoChild extends DemoParent,Demo{

}
void main(){

}
*/
// Error: The superclass, 'DemoParent', has no unnamed constructor that takes no arguments.

// Each class defination can have at most one extends clause.