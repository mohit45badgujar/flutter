import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Portfolio(),
      debugShowCheckedModeBanner: false,
        
        //body:Portfolio(),
        
    );
    
  }
}

class Portfolio extends StatefulWidget{
  const Portfolio ({super.key});
  @override
  State<Portfolio>createState()=>_PortfolioState();
}
class _PortfolioState extends  State <Portfolio>{
  int counter=-1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Portfolio"),
        backgroundColor: const Color.fromARGB(255, 172, 245, 245),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState((){
            counter++;
          });
        },
        child: const Text("Next"),
      ),
      body: Container(
        color:  const Color.fromARGB(255, 58, 225, 244),
        child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // (counter >= 0)
            // ? Container(
            //   height: 600,
            //   width: 20,
            //   color: Colors.black,
            // )
            // :Container(),

            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                  //width: 250,
                  //color: Colors.orange,
                ),
                

                (counter >= 0)
                ?const Text("Name : Mohit Bhimrao Badgujar",style: TextStyle(fontSize: 16),) 
                            
                :const Text(""),

                
                const SizedBox(
                  height: 20,
                 // width: ,
                  //color: Colors.orange,
                ),
               

                (counter >=1)
                ? Container(
                  height: 140,
                  width: 140,
                  color: const Color.fromARGB(255, 58, 225, 244),
                  child:
                  // ignore: unnecessary_string_escapes
                   Image.network("https://media.licdn.com/dms/image/D4D03AQHtRvztAODXRQ/profile-displayphoto-shrink_800_800/0/1664984428561?e=2147483647&v=beta&t=owAaQVSKwAcgG1EVlrVbMOdDqvMtp7uXGhVsaamgxTA")
                                  
                
                )
                :Container(),
             
                const SizedBox(
                  height: 20,
                  //width: 250,
                  //color: Colors.orange,
                ),
               

                (counter >= 2)
                ?const Text("College : JSPM Narhe Technical Campus",style: TextStyle(fontSize: 16),) 
                            
                :const Text(""),

                const SizedBox(
                  height: 20,
                 // width: ,
                  //color: Colors.orange,
                ),
                

                (counter >=3)
                ? Container(
                  height: 140,
                  width: 140,
                  color: const Color.fromARGB(255, 58, 225, 244),
                  child: 
                  // ignore: unnecessary_string_escapes
                   Image.network("https://images.shiksha.com/mediadata/images/1596772689phpwCy3LM.jpeg")
                                   
                
                )
                :Container(),

              
                const SizedBox(
                  height: 20,
                  //width: 250,
                  //color: Colors.orange,
                ),
                

                (counter >= 4)
                ?const Text("Dream Company : Codify",style: TextStyle(fontSize: 16),) 
                            
                :const Text(""),

                const SizedBox(
                  height: 20,
                 // width: ,
                  //color: Colors.orange,
                ),
               

                (counter >=5)
                ? Container(
                  height: 140,
                  width: 140,
                  color: Colors.white,
                  child: 
                  // ignore: unnecessary_string_escapes
                  Image.network("https://img.shgstatic.com/clutch-static-prod/image/scale/50x50/s3fs-public/logos/46f591f7a9fe7f11892b8c93b07d8403.jpg")
                                    
                
                )
                :Container(),


              ],
            ),
          ],
        ),
      ),
    );

  }

  
}