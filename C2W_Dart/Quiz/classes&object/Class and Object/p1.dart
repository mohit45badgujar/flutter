class Student {
  String name="Rajesh";
  int age=28;

  Student(String name,int age){
    this.age=age;
    this.name=name;
  }
  void display(){
    print('Name:$name,Age:$age');
  }
}
void main(){
  var student1=new Student('Ganesh',20);
  student1.age+=1;
  student1.display();
}