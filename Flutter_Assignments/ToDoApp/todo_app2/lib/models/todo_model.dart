

class TODOModel {
  final String title;
  final String description;
  final DateTime date;

  TODOModel({
    required this.title,
    required this.description,
    required this.date,
  });
}