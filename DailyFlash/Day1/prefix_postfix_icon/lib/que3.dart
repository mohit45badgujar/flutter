// 

import 'package:flutter/material.dart';

class Question3 extends StatefulWidget {
  const Question3({super.key});
  @override
  State createState()=> _Question3State();
}

class _Question3State extends State {
  
  @override
  Widget build(BuildContext context){
    
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.blue,
        
        title: const Text("IconPlace"),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30)
          )
        ),
        centerTitle: true,
        
        
      ) ,
    );
  }
}