import 'package:flutter/Material.dart';

void main()=>runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override 
  Widget build(BuildContext context){
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home : TextQuiz(),
    );
  }
}

class TextQuiz extends StatefulWidget {
  const TextQuiz({super.key});
  
  @override
  State<TextQuiz>createState()=>_TextQuizState();
}

class _TextQuizState extends State <TextQuiz> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title:const Text(
          "Text Quiz"),
        backgroundColor: Colors.pink,
      ),
      body:  Row(
        //crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        
      
        children: [
          Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[
              const SizedBox(
               height: 20,
              ),
              const Text ("Question : 1 / 10"),

              const SizedBox(
                height: 30,                
              ),

              const Text("Question 1: What is Flutter ?"),

              const SizedBox(
                height: 30,
              ),

              ElevatedButton(
                  onPressed: (){
                  },
                  child:const Text ("Option 1"),                
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed:(){

                },
                child: const Text("Option 2"),
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed:(){},
                child:const Text("Option 3"),
              ),

              const SizedBox(
                height: 20,
              ),

              ElevatedButton(
                onPressed: (){},
                child:const Text("Option 4"),
              ),

              const SizedBox(
                height: 50,
              ),
              
              
             

            ],
          ),
      
        ],
      ),
    

      
    ); 
  }

}
