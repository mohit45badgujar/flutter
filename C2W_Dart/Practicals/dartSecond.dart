
int palindromCnt(int start,int end){
  int count=0;
  for(int i=start;i<=end;i++){
    int num=i;
    int num1=num;
    int rev=0;
    while(num != 0){
      int rem=num % 10;
      rev=rev*10+rem;
      num=num ~/ 10;
    }
    if(num1==rev){
      count++;
    }
  }

  return count;
}