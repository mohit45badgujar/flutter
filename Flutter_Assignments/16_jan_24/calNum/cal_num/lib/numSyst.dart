import 'package:flutter/material.dart';

class NumberSystem extends StatefulWidget{
  const NumberSystem({super.key});

  @override
  State<NumberSystem>createState() => _NumbSysState();

}
class _NumbSysState extends State<NumberSystem>{
  int count=0;
  void _parindromCnt(){
    setState(() {
      var num=[121,45,28,55,333];
      //int count=0;
      for(int i=0;i<num.length;i++){
        int temp=i;
        int rev=0;
  
        while(temp > 0){
          rev=(rev*10)+temp % 10;
          temp=temp;
        }
        if(rev==i){
          print(i);
          count+=1;
        }
      }
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: const Text("Count Number System Count" ),
      ),
      body: SizedBox(
        width:double.infinity,
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed:(){
                _parindromCnt();
                setState((){});
              },
              child: const Text("Check Palindrom "),
            ),
            const SizedBox(
              height: 20,
            ),
            Text("$count Numbers are Palindrome")
          ], 
        ),
      )
    );
  }

}