/*
abstract class Devloper {
  void devlop(){
    print("We Build Software");
  }
  void devType();
}
class MobileDev implements Devloper {
  void devType(){
    print("Flutter Dev");
  }
}
class WebDev extends Devloper {
  void devType(){
    print("Web Dev");
  }
}
void main(){
  Devloper obj1=new MobileDev();
  obj1.devlop();
  obj1.devType();

  Devloper obj2=new WebDev();
  obj2.devlop();
  obj2.devType();

  WebDev obj3=new WebDev();
  obj3.devlop();
  obj3.devType();

  
}
Error: The non-abstract class 'MobileDev' is missing implementations for these members:
Context: 'Devloper.devlop' is defined here.
*/