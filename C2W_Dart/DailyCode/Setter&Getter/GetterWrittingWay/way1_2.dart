import 'way1_1.dart';

void main(){
  Demo obj=new Demo(10,"Kanha",10.2);

  print(obj.getX()); // 10
  print(obj.getX); // Closure: () => int? from Function 'getX':.
  print(obj.str);  // Kanha
  print(obj.getSal()); // 1.2
  print(obj.getSal); // Closure: () => double? from Function 'getSal':.
  print(obj.getStr()); // Kanha
  print(obj.getStr); // Closure: () => String? from Function 'getStr':.
}