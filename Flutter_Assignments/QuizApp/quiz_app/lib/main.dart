import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: QuizApp(),
      
    );
  }
}
class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override

  State createState()=> _QuizAppState();
}

class _QuizAppState extends State {

  List<Map> allQuestions=[
    {
      "question": " Who is the Founder of MicroSoft?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "correct":2,
    },
    {
      "question": " Who is the Founder of Apple?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "correct":0,
    },
    {
      "question": " Who is the Founder of Amazon?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "correct":2,
    },
    {
      "question": " Who is the Founder of Tesla?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "correct":3,
    },
    {
      "question": " Who is the Founder of Google?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
      "correct":1,
    },

  ];
  
  bool questionScreen=true;
  int queNo=0;
  int selected=-1;
  int result=0;
  String msg="";


  MaterialStateProperty<Color>giveColor(int index){
    if(selected !=-1){
      if(index==allQuestions[queNo]["correct"]){
        return const MaterialStatePropertyAll(Colors.green);
      }else if(index == selected){
        return const MaterialStatePropertyAll(Colors.red);
      }else{
        return const MaterialStatePropertyAll(Colors.blue);
      }
    }
    return const MaterialStatePropertyAll(Colors.blue);
  }

  void validatePage(){
    if(selected == -1){
      msg="Please Select an option";
    }else{
      if(selected==allQuestions[queNo]["correct"]){
        result++;
      }
      if(queNo==allQuestions.length-1){
        questionScreen=false;
        msg="";
        selected=-1;
      }else{
        msg="";
        queNo++;
        selected=-1;
      }
    }setState(() {
      
    });
  }

  SizedBox printResult(){
    if(result ==0){
      return SizedBox(
        width:300,
        child: Column(
          children: [
            const Text("Upps...",
            style: TextStyle(fontSize:30,
            fontStyle: FontStyle.italic),
            ),
            Text("You Have Scored $result out of ${allQuestions.length}",
            style: const TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.italic
            ),
            ),
            const Text("Better Luck Next Time...",
            style: TextStyle(fontSize: 20,
            fontStyle: FontStyle.italic
            ),
            ),
            
        ]),
      

      );
    }else{
      return SizedBox(
        width:300,
        child: Column(
          children: [
            Image.network("https://media.tenor.com/4PTN0FggEYgAAAAj/kupa.gif",
            height: 300,
            width: 300,
            ),
            const Text(
              "Congratulations !!!",
              style: TextStyle(fontSize: 30,
              fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 10),
            Text(
              "You Have Scored $result out of ${allQuestions.length}",
              style: const TextStyle(fontSize: 20,
              fontStyle: FontStyle.italic),
              ),
          ]),
      );
    }
  }

  Scaffold isQuestionScreen(){
    if(questionScreen){
      return Scaffold(
        appBar:AppBar(
          backgroundColor:Colors.blue,
          title:const Text("Quiz App",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w600,
           
          )),
          centerTitle: true,
         
         ),

         floatingActionButton: FloatingActionButton(
          onPressed:() {
            validatePage();
          },
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.orange,
          ),
         ),


         body: Column(
          children: [
            const SizedBox(
              height: 60,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                 Text(
                  "Questions :${queNo+1}/${allQuestions.length}",
                  style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                  ),
                ),               
              ],
            ),
            const SizedBox(
              height: 30,
            ),

            SizedBox(
              width: 300,
              //height: 50,
              child: Text(
                allQuestions[queNo]["question"],
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),

            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: giveColor(0),
                ),
                onPressed:(){
                
                  if(selected==-1){
                    selected=0;
                  }
                  setState((){}
                  );    
                },

              
                child: Text(
                  "A.${allQuestions[queNo]["options"][0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  )),
                ),
            ),
            
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: giveColor(1),
                ),
                onPressed:(){
                
                  if(selected==-1){
                    selected=1;
                  }
                  setState((){}
                  );    
                },
                child: Text(
                  "B.${allQuestions[queNo]["options"][1]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  )),
                ),
            ),
                
             const SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: giveColor(2),
                ),
                onPressed:(){
                
                  if(selected==-1){
                    selected=2;
                  }
                  setState((){}
                  );    
                },
                child: Text(
                  "C.${allQuestions[queNo]["options"][2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  )),
                ),
            ),   
            
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 40,
              width: 350,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: giveColor(3),
                ),
                onPressed:(){
                
                  if(selected==-1){
                    selected=3;
                  }
                  setState((){}
                  );    
                },
                child: Text(
                  "D.${allQuestions[queNo]["options"][3]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  )),
                ),
            ),
            
            const SizedBox(height: 10,),
            Text(msg,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.normal
            )),
          ],
         ),
      );
    }else{
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text("Quiz App",
          style: TextStyle(
            fontSize: 30,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.w600,
          )),
          centerTitle: true,
        ),

        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children:[
                printResult(),

                const SizedBox(
                  height: 30,
                ),

                ElevatedButton(onPressed:(){
                  setState(() {
                    
                      questionScreen=true;
                      queNo=0;
                      result=0;
                    });
                  },
            
                  style:const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.blue)
                  ),
                  child:const Text("Reset")
                  ),     
              ],
            )
          ],
        ));
    }
  }
              
  @override
  Widget build(BuildContext context){
    return isQuestionScreen();
  }
}