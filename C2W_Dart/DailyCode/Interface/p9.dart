abstract class InterfaceDemo1 {
  void m1(){
    print("In m1-interface1");
  }
 
}
abstract class InterfaceDemo2 {
  void m1(){
    print("in m1-interface2");
  }
}
class demo implements InterfaceDemo1,InterfaceDemo2 {
  void m1(){
    print("In m1");
  }
}
void main(){
  demo obj=new demo();
  obj.m1();
}