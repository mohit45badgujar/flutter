class Demo{
  void fun(){
    print("In fun");
  }
}
void main(){
  Demo obj=new Demo();
  obj.fun();

  obj=null;  // Error: The value 'null' can't be assigned to a variable
                 //    of type 'Demo' because 'Demo' is not nullable.

  obj.fun();
}