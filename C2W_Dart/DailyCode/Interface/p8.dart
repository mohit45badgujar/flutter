abstract class IFC {
  void material() {
    print("Indian Material");
  }
}
class IndianFC implements IFC {
  void material(){
  }
  void teste(){
    print("Indian Teste");
  }
}
class EUFC extends IFC {
  void material(){
    print("Indian Material");
  }
  void teste(){
    print("Europian Teste");
  }
}
void main(){
  IndianFC obj=new IndianFC();
  obj.material();
  obj.teste();
}
// o/p : Indian Teste