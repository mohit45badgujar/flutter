class Parent {
  int x=10;
  String str="surname";

  void parentDisplay(){
    print("In Parent Display");
  }
}
class Child extends Parent {
  int y=20;
  String str2="name";

  void childDisp(){
    print(y);
    print(str2);
  } 
}

void main(){
  Parent obj=new Parent();
 /*
  print(obj.y);     Error: The getter 'y' isn't defined for the class 'Parent'.
  print(obj.str2);    Error: The getter 'str2' isn't defined for the class 'Parent'.
  obj.childDisp();    Error: The method 'childDisp' isn't defined for the class 'Parent'.
  */
 
}
/*
10
surname
In Parent Display
*/