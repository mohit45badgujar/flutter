abstract class Parent {
  void property(){
    print("Gold,Cars,Farm,Flats");
  }
  void carrer();
  void marry();
}
void main(){
  //Parent obj=new Parent();  Error: The class 'Parent' is abstract and can't be instantiated.
  //obj.property();
}
