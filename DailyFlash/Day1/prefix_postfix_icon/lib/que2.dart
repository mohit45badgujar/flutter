// Create a AppBAr give a color  of your choice to the AppBar and then add an icon at the start of the AppBar and 3 icon at the end of AppBar.

import 'package:flutter/material.dart';

class Question2 extends StatefulWidget {
  const Question2({super.key});
  @override
  State createState()=> _Question2State();
}

class _Question2State extends State {
  
  @override
  Widget build(BuildContext context){
    
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.blue,
        leading: const Icon(Icons.menu),
        title: const Text("IconPlace",
        ),
        centerTitle: true,
        actions: const [
          Icon(
            Icons.favorite,
          ),
          Icon(
            Icons.message,
          ),
          Icon(
            Icons.search,
          ),
        ],
      ) ,
    );
  }
}