
class Demo {
  int? _x;
  String? str;
  double? _sal;

  Demo(this._x,this.str,this._sal);

  get getX => _x;
  String? get getStr => str;
  get getSal {
    return _sal;
  }
}