import 'dart:io';
bool isprimeNum(int x){
  int count=0;
  for(int i=1;i<=x;i++){
    if(x%i==0){
      count++;
    }    
  }
  if(count == 2)
    return true;
  else
    return false;
  
}
void main(){
  int? row=int.parse(stdin.readLineSync()!);
  int x=2;
  for(int i=1;i<=row;i++){
    for(int j=row-1;j>=i;j--){
      stdout.write("-  ");
    }
    for(int k=1;k<=i;){
      if(isprimeNum(x)){
        stdout.write("${x} ");
        k++;
      }
      x++;
    } 
    print("");
  }
}