import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: WidgetsApp(),
    );
  }
}

class WidgetsApp extends StatefulWidget {
  const WidgetsApp({super.key});
  @override
  State<WidgetsApp>createState()=>_WidgetsAppState();
}

class _WidgetsAppState extends State <WidgetsApp> {
  List<String>imageList=[
    
    "https://static.punjabkesari.in/multimedia/16_29_4974356986.jpg",
    "https://touristplacesnearme.co.in/wp-content/uploads/2023/08/Siddhivinayak.jpg",
    "https://content.jdmagicbox.com/comp/raigad-maharashtra/j9/9999p2142.2142.140114102116.p2j9/catalogue/ballaleshwar-ganpati-temple-pali-raigad-maharashtra-temples-s4j9zn5a7u.jpg?clr=#662900",
    "https://media-cdn.shreeganesh.com/wp-content/uploads/2020/10/Varada-Vinayak-Temple.jpg",
    "https://i.pinimg.com/474x/19/11/8e/19118e878e700335df1607e1b5f70012.jpg",
    "https://www.ashtavinayak.in/images/lenyadri/lenyadri-girijatmaj-ganpati-ashtavinayak.jpg",
    "https://d2vpks47p5k2of.cloudfront.net/wp-content/uploads/2019/06/Vighneshwar-Temple.jpg",
  ];
  List<String> info=["Mayureshwar Ganpati,Morgaon","Siddhivinayak Ganpati,Siddhatek","Ballaleshwar Ganpati,Pali","Varadvinayak Ganpati,Mahad","Chintamani Ganpati,Theur","Girijatmaj Ganpati,Lenyadri","Vigneshwar Ganpati,Ozar"];
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor:Colors.orange,
      appBar:AppBar(
        title:const Text(
          "Ashtavinayak Darshan",
          
        ),
        centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 204, 95, 131),
      ),
      body:ListView.builder(
        
        shrinkWrap: true,
                itemCount: imageList.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.all(10),
                    child:Column(children: [
                      Image.network(
                      imageList[index],
                    ),
                    Text(info[index]),
                    ],) 
                    
                  );
                }  
        )
    );
  }
}
      
      
      
      
      
      
      
      /*SingleChildScrollView(
      child:Column(
        
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,

            children: [
              const SizedBox(
                height: 20,
              ),

              const Text("Ashtavinayak Darshan",style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),

              const SizedBox(
                height: 20,
              ),
              Column(children: [
                ListView.builder(
                shrinkWrap: true,
                itemCount: imageList.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.all(10),
                    child: Image.network(
                      imageList[index],
                    ),
                  );
                }                
              ),
              ],)
              
            ],
          ),
          
        ],),
    ));
  }
}*/