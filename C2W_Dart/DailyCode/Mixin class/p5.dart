/*
mixin Demo1 {
  void fun1(){
    print("In fun1-Demo1");
  }
  void fun2();
  
}
mixin class Demo2 {
  void fun3(){
    print("In Demo2-fun3");
  }
  void fun4();
}
class DemoChild implements Demo1,Demo2 {
  void fun2(){
    print("In Demo Child-fun2");
  }
  void fun4(){
    print("In Demo Child-fun4");
  }
}
void main(){
  DemoChild obj=new DemoChild();
  obj.fun2();
  obj.fun4();
}
*/
/*
Error: The non-abstract class 'DemoChild' is missing implementations for these members:
 - Demo1.fun1
 - Demo2.fun3

 Error : 'Demo2.fun1' is defined here.
 Error : 'Demo2.fun3' is defined here.
 */