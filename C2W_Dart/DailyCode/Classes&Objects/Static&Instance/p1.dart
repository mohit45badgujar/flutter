
class Company {
  int empCount=500;
  String compName="Google";
  String loc="Pune";

  void compInfo(){
    print(empCount);
    print(compName);
    print(loc);
  }
}
void main(){
  // Way-1:
  Company obj1=new Company();
  obj1.compInfo();

  //Way-2:
  Company obj2=Company();
  obj2.compInfo();

  //Way-3:
  new Company();
  new Company().compInfo();

  //Way-4:
  Company();
  Company().compInfo();
}