import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: IndiaFlag(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class IndiaFlag extends StatelessWidget {
  const IndiaFlag({super.key});

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        
        backgroundColor: const Color.fromARGB(255, 255, 119, 0),
        elevation: 4,
        centerTitle: true,
        title: const Text("*Republic Day*"),
      ),
      
      body: Container(
          height: double.infinity,
          width: double.infinity,
          
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              
              colors:[
                Color.fromARGB(255, 245, 66, 1),
               Color.fromARGB(255, 245, 115, 1),
               Colors.white,
                Color.fromARGB(255, 181, 171, 210),
                Colors.white,
                Colors.green,
                Color.fromARGB(255, 1, 67, 3),
              ],
              
            ),
          ),
          
          child: Column(
            
            mainAxisAlignment: MainAxisAlignment.center,
            
            children: [
              const Text(
                "🧡🤍💚Happy Republic Day🧡🤍💚",style: TextStyle(
                  fontSize: 22,
                ),
              ),
              const SizedBox(
                height: 80,
              ),
              Container(
                height: 50,
                width: 230,
                margin: const EdgeInsets.only(left: 10),
                decoration: const BoxDecoration(
                  color: Colors.orange,
                  border: Border(
                    top: BorderSide(width: 2.0, color: Colors.black),
                    //bottom: BorderSide(width: 2.0,color: Colors.black),
                    left:BorderSide(width: 2.0,color: Colors.black),
                    right: BorderSide(width: 2.0,color: Colors.black),
                  ),
                ),
              ),
              Container(
                height: 50,
                width: 230,
                margin: const EdgeInsets.only(left: 10),
                
                decoration: const BoxDecoration(
                  border: Border(
                    //top: BorderSide(width: 2.0, color: Colors.black),
                    //bottom: BorderSide(width: 2.0,color: Colors.black),
                    left:BorderSide(width: 2.0,color: Colors.black),
                    right: BorderSide(width: 2.0,color: Colors.black),
                  ),
                
                  color:Colors.white,
                  
                 ), child:Image.network(
                    "https://pbs.twimg.com/tweet_video_thumb/CMxCEuJUAAEDZVK.png",
                  ),
                
                ),
              
              Container(
                height: 50,
                width: 230,
                margin: const EdgeInsets.only(left: 10),
                decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 0, 61, 3),
                  border: Border(
                    //top: BorderSide(width: 2.0, color: Colors.black),
                    bottom: BorderSide(width: 2.0,color: Colors.black),
                    left:BorderSide(width: 2.0,color: Colors.black),
                    right: BorderSide(width: 2.0,color: Colors.black),
                  ),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
             
              const SizedBox(
                height: 20,
              ),
              const Text(
                "     स्वातंत्र्याची ध्वजा फडकते सूर्य तळपतो प्रगतीचा,भारतभूमीच्या पराक्रमाला मुजरा हा मानाचा. उत्सव तीन रंगांचा,आभाळी आज सजला,नतमस्तक मी त्या सर्वांसाठी,ज्यांनी भारत देश घडविला...भारत देशाला मानाचा मुजरा!. ",
                style: TextStyle(fontSize: 20),

                
              ),
                
              
              
            ]),
        
      ),
    );
  }
}