import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: TodoListDemo(),
    );
  }

}
class TodoListDemo extends StatefulWidget{
  const TodoListDemo({super.key});

  @override
  State createState() => _TodoListDemoState();

}
class _TodoListDemoState extends State {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "TextFileDemo",
          style: GoogleFonts.quicksand(
            color:Colors.amber,
            fontSize:20.0,
            fontWeight:FontWeight.w500,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ListView.builder(
        itemCount:4,
        itemBuilder:(BuildContext context ,int value  ){
          return Container(
            width: double.infinity,
          );
        }
      ),
    );
  }
}