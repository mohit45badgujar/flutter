import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: DisplayListView(),
    );
  }
}
class DisplayListView extends StatefulWidget{
  const DisplayListView({super.key});
  @override
  State createState()=>_DisplayListView();
  
}
class _DisplayListView extends State<DisplayListView> {
  List<String>imageList=[
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9x_QMqUNUwye27r93ZTXwrMVfZlqRtgut5Q&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMLiEjfLMzOVXEm087UF_MUGzItzgshd4N8w&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSic2q5u5XcEwTHvPh4Sjp1hp-UPt6edSYcDQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9x_QMqUNUwye27r93ZTXwrMVfZlqRtgut5Q&usqp=CAU",
  ];
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        centerTitle: true,
        title: const Text("List View Builder Demo"),
        backgroundColor: Colors.red,
      ),
      body: ListView.builder(
        itemCount: imageList.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.all(10),
            child: Image.network(
              imageList[index],
            ),
          );
        }
      ),
    );
  }
}
        