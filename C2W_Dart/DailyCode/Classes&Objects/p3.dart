import 'dart:io';
class Employee {
  String? empName='Shashi';
  int empId=120;
  double? Salary=1.2;

  void empInfo(){
    print("Employee Name= $empName");
    print("Employee Id= $empId");
    print("Employee salary= $Salary");
  }
}
void main(){
  Employee obj=new Employee();
  obj.empInfo();

  print(" ");
  print("Enter Employee id");
  obj.empId=int.parse(stdin.readLineSync()!);

  print("Employee name");
  obj.empName=stdin.readLineSync();

  print("Enter employee salary");
  obj.Salary=double.parse(stdin.readLineSync()!);

  obj.empInfo();
}

/*
Employee Name= Shashi
Employee Id= 120
Employee salary= 1.2

Enter Employee id
110
Employee name
Mohit
Enter employee salary
1.0
Employee Name= Mohit
Employee Id= 110
Employee salary= 1.0
*/