/*
class Demo{
  int x=10;
  void Demo(){
    print("In Constr");
  }
}
void main(){
  Demo obj=new Demo();
}
*/
//  Error: Constructors can't have a return type.
//  Try removing the return type.