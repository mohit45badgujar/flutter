// Write a dart prog, calculate electricity bill of a house based on following criteria
// for first 90 units:No charge
// 90 to 180 units: 6 rs per unit
// 180 to 250 units: 10 rs per unit
// Above 250 units: 15 rs per unit


void main(){
  var x=120;
  if(x<90){
    print("NO charge");
  }else if(x>=90 || x<180){
    print(x*6);
  }else if(x>=180 || x<250){
    print(x*10);
  }else{
    print(x*15);
  }
}