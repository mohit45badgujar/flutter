import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body:IndiaFlag(),
      ),
    );
  }
}

class IndiaFlag extends StatefulWidget{
  const IndiaFlag ({super.key});
  @override
  State<IndiaFlag>createState()=>_IndiaFlagState();
}
class _IndiaFlagState extends  State <IndiaFlag>{
  int counter=-1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("India Flag"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState((){
            counter++;
          });
        },
        child: const Text("Add"),
      ),
      body: Container(
        color: Colors.grey[300],
        child:Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            (counter >= 0)
            ? Container(
              height: 600,
              width: 20,
              color: Colors.black,
            )
            :Container(),

            Column(
              children: [
                (counter >=1)
                ? Container(
                  height: 50,
                  width: 250,
                  color: Colors.orange,
                )
                :Container(),

                (counter >= 2)
                ? Container(
                  height: 50,
                  width: 250,
                  color: Colors.white,
                  child: (counter >= 3)
                  ? Image.network(
                    "https://upload.wikimedia.org/wikipedia/commons/e/e4/Spinning_Ashoka_Chakra.gif",
                  )
                  : Container(),                  
                )
                :Container(),

                (counter >=4)
                ? Container(
                  height: 50,
                  width: 250,
                  color: Colors.green,
                )
                :Container(),
              ],
            ),
          ],
        ),
      ),
    );

  }

  
}