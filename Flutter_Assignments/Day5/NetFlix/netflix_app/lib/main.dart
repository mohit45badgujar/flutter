import 'package:flutter/material.dart';
//import 'package:netflix/Assignment5.dart';


void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home:Assignment5(),
    );
  }
}
class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});
  @override
  State<Assignment5>createState()=>_AssignmentState();
}
class _AssignmentState extends State<Assignment5>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title:const Text(
          "NETFLIX",
          style: TextStyle(
            color: Colors.black,
            fontSize: 30,
          ),
        ),
      ),
      
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          
          crossAxisAlignment:CrossAxisAlignment.start,
          children:[
            const SizedBox(
              width:30,
              height: 20,
            ),
            const Text(
              "Movies",
              style: TextStyle(
                fontSize:20,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
            
              child:Row(
                mainAxisAlignment:MainAxisAlignment.start,
                crossAxisAlignment:CrossAxisAlignment.start,
                children:[
                  
                  Container(
                    color:Colors.white,
                    width:295,
                    //height:350,
                    child:Image.network(
                      "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
                      width:190,
                      //height: 260,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.white,
                    width:325,
                    child:Image.network(
                      "https://e1.pxfuel.com/desktop-wallpaper/451/476/desktop-wallpaper-the-best-bollywood-movies-now-streaming-on-netflix-and-amazon-prime-video-bollywood-movie-poster-2021.jpg",
                      // width: 300,
                      // height: 280,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.white,
                    width:350,
                    child:Image.network(
                      "https://greenmangomore.com/wp-content/uploads/2023/12/Hrithik-Movie-Fighter-Posrter.jpg",
                      width:300,
                    
                    ),
                  ),
              
                ],
              ),
            ),
            const SizedBox(
              width:30,
              height: 20,
            ),
            const Text(
              "Series",
              style: TextStyle(
                fontSize:20,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
            
              child:Row(
                mainAxisAlignment:MainAxisAlignment.start,
                crossAxisAlignment:CrossAxisAlignment.start,
                children:[
                  
                  Container(
                    color:Colors.white,
                    width:180,
                    child:Image.network(
                      "https://e1.pxfuel.com/desktop-wallpaper/322/698/desktop-wallpaper-upcoming-hindi-film-movie-posters-bollywood-2022-movie.jpg",
                      width:230,                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.amber,
                    width:180,
                    child:Image.network(
                      "https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943",
                        width:230,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.amber,
                    width:180,
                    child:Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmWsnIy6EvoAMZTL4pS07G7EgIiS6z8i_kTvhK91b0c0a0AxBMcOTje0JGWhtHg4SnX6E&usqp=CAU",
                      width:230,
                    
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width:30,
              height: 20,
            ),
            const Text(
              "Most Popular",
              style: TextStyle(
                
                fontSize:20,
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
            
              child:Row(
                mainAxisAlignment:MainAxisAlignment.start,
                crossAxisAlignment:CrossAxisAlignment.start,
                children:[
                  
                  Container(
                    color:Colors.white,
                    width:130,
                    child:Image.network(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0kR0gMemRl9ylPTzmmuQQVb10vo8n7kXL7BeHkeo_4lmJS56C8-WKIy_GYK12wnEmPlc",
                      width:230,
                      //height:230 ,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.white,
                    width:120.5,
                    child:Image.network(
                      "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                      width:230,
                      //height:230,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.white,
                    width:115,
                    child:Image.network(
                      "https://i.redd.it/6t0dr1c9fq6a1.jpg",
                      width:220,
                      //height: 230,
                    
                    ),
                  ),
                  const SizedBox(
                    width:30,
                    height: 20,
                  ),
                  Container(
                    color:Colors.white,
                    width:138,
                    child:Image.network(
                      "https://www.scrolldroll.com/wp-content/uploads/2023/03/chor-nikal-ke-bhaaga-hindi-movies-on-netflix-in-2023.jpg",
                      width:190,
                      //height: 230,
                    
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      
    );
  }
}