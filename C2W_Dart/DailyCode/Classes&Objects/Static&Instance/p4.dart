// static Variables

class Demo {
  int x=10;
  static int y=20;

  void printData(){
    print(x);
    print(y);

  }
}
void main(){
  Demo obj1=new Demo();
  obj1.printData(); // 10
                    //20
  Demo obj2=Demo(); 
  obj2.printData();//10
                   //20
}