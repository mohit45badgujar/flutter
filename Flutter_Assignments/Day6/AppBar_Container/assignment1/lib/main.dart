import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment1(),
    );
  }
}

class Assignment1 extends StatefulWidget{
  const Assignment1({super.key});
  
  @override
  State<Assignment1>createState()=>_AssignmentState();
}
class _AssignmentState extends State<Assignment1> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: const Text(
          "Day 6 Assignment 1",
          
        ),
        actions: const[
          Icon(
            Icons.search_outlined,
          ),
          Icon(
            Icons.favorite_outline_rounded,
          )
        ],
      ),
    );
  }

}
