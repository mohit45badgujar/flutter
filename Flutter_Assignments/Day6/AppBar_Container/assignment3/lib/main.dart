import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment3(),
    );
  }
}

class Assignment3 extends StatefulWidget{
  const Assignment3({super.key});
  
  @override
  State<Assignment3>createState()=>_AssignmentState();
}
class _AssignmentState extends State<Assignment3> {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text(
          "Hello Core2Web",
        ),
        centerTitle: true,
          
        
        actions: const[
          Icon(
            Icons.search_outlined,
          ),
          Icon(
            Icons.more_vert,
          )
        ],
      ),
      body: Center(
        child:Container(
          width: 360,
          height: 150,
          color: Colors.blue,
        ),
      ),
    );
  }

}
