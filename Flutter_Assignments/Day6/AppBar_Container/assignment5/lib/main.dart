// ignore_for_file: unnecessary_string_escapes

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:Assignment5(),
    );
  }
}

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});
  @override
  State<Assignment5>createState()=>_AssignmentState();
}

class _AssignmentState extends State<Assignment5>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title:const Text (
          "Asset Image",
        ),
      ),
      body: Column(
        children: [
          Image.asset(
            "F:\flutter\Flutter_Assignments\Day6\AppBar_Container\assignment5\lib\asset\rohit1.jpeg",
          ),

        ],
      ),
    );
  }
}
