
class Demo {
  int? _x;
  String? _str;

  Demo(this._x,this._str);

  void disp(){
    print(_x);
    print(_str);
  }
}
void main(){
  Demo obj1=new Demo(10,"Om");
  obj1.disp();

  obj1._x=15;
  obj1._str="Jaggu";

  obj1.disp();
}
/*
10
Om
15
Jaggu*/