/*
abstract class Devloper {
  void devlop(){
    print("We Build Software");
  }
  void devType();
}
class MobileDev extends Devloper {
  void devType(){
    print("Flutter Dev");
  }
}
class WebDev extends Devloper {
  void devType(){
    print("Web Dev");
  }
}
void main(){
  Devloper obj1=new MobileDev();
  obj1.devlop();
  obj1.devType();

  Devloper obj2=new WebDev();
  obj2.devlop();
  obj2.devType();

  WebDev obj3=new WebDev();
  obj3.devlop();
  obj3.devType();

  Devloper obj4=new Devloper(); Error: The class 'Devloper' is abstract and can't be instantiated.
  obj4.devlop();
  obj4.devType();
}
*/