import 'package:flutter/material.dart';

class Medicine {
  int? medcineId;
  final String medicineName;
  final int durationDays;
  final int noOfDosePerDay;
  final List<String> setTime;
  Medicine({
    this.medcineId,
    required this.medicineName,
    required this.durationDays,
    required this.noOfDosePerDay,
    required this.setTime,
  });
}

List<Medicine> medicineList = [
  Medicine(
      medcineId: 1,
      medicineName: "ColdMac",
      durationDays: 2,
      noOfDosePerDay: 2,
      setTime: ["5", "3"])
];

class MedicineAlert extends StatefulWidget {
  const MedicineAlert({super.key});
  @override
  State createState() => _Medicine();
}

class _Medicine extends State {
  //controllers
  TextEditingController name = TextEditingController();
  TextEditingController duration = TextEditingController();
  TextEditingController noOfDose = TextEditingController();
  TextEditingController time = TextEditingController();
  Color defaultColor = Colors.green;

  void showSheet() {
    int doseCount = 0;
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return Container(
          padding:
              const EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: name,
                decoration: InputDecoration(
                  hintText: "  Enter Medicine Name",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: defaultColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: defaultColor,
                    ),
                  ),
                ),
                cursorColor: defaultColor,
              ),
              const SizedBox(height: 5),
              TextField(
                controller: duration,
                decoration: InputDecoration(
                  hintText: "  Enter Duration (in days)",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: defaultColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: defaultColor,
                    ),
                  ),
                ),
                cursorColor: defaultColor,
              ),
              const SizedBox(height: 5),
              TextField(
                controller: noOfDose,
                decoration: InputDecoration(
                  hintText: " Total dose per day",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: defaultColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: defaultColor,
                    ),
                  ),
                ),
                cursorColor: defaultColor,
              ),
              const SizedBox(height: 5),
              TextField(
                controller: time,
                decoration: InputDecoration(
                  hintText: "Time",
                  border: InputBorder.none,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(color: defaultColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: defaultColor,
                    ),
                  ),
                ),
                cursorColor: defaultColor,
                onTap: () async {
                  doseCount = int.parse(noOfDose.text);
                  List<String> timings = [];
                  for (int i = 0; i < doseCount; i++) {
                    final TimeOfDay? picked = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                    );

                    if (picked != null) {
                      setState(() {
                        timings.add(picked.format(context));
                      });
                    }
                  }
                  setState(() {
                    time.text = timings.join(" , ");
                  });
                },
              ),
              const SizedBox(height: 6),
              GestureDetector(
                onTap: () {},
                child: Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      color: defaultColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: const Text("Set Reminder")),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showSheet();
        },
        backgroundColor: defaultColor,
        child: const Icon(Icons.add),
      ),
      body: Column(
        children: [
          Container(
              padding: const EdgeInsets.all(20),
              margin: const EdgeInsets.all(10),
              width: double.infinity,
              decoration: BoxDecoration(
                color: defaultColor,
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(color: Color.fromARGB(255, 125, 200, 127))
                ],
              ),
              child: const Text("Medicine Reminders",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700))),
          const SizedBox(height: 10),
          Expanded(
              child: ListView.builder(
            itemCount: medicineList.length,
            itemBuilder: (context, index) {
              return Container(
                height: 80,
                width: 140,
                margin: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: defaultColor,
                        blurRadius: 6.0,
                        spreadRadius: 1.0,
                      )
                    ],
                    borderRadius: BorderRadius.circular(10)),
                child: Row(children: [
                  Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.network("assets/medicine.png")),
                  const SizedBox(width: 5),
                  Text(
                    medicineList[index].medicineName,
                    style: const TextStyle(
                        fontSize: 25, fontWeight: FontWeight.w500),
                  )
                ]),
              );
            },
          ))
        ],
      ),
    );
  }
}
