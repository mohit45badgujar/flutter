class Parent {
  int x=10;
  String str="surname";

  void parentDisplay(){
    print("In Parent Display");
  }
}
class Child extends Parent {

}
void main(){
  Child obj=new Child();
  print(obj.x);
  print(obj.str);
  obj.parentDisplay();
}
/*
10
surname
In Parent Display
*/