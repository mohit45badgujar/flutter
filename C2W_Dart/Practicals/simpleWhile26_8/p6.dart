// WAP to print Square of odd digit btn 20 to 10 in reverse order

 void main(){
  int y=20;
  while(y>=10){
    if(y%2!=0){
      print(y*y);
    }
    y--;
  }
}