import "dart:io";

void main(){
  print("Start main");
  print("Enter value");
  try {
    int? data=int.parse(stdin.readLineSync()!);
    print(data);
  } on FormatException {
    print("Exception Handled");
  
  }catch(ex){
    
    print(ex);
  }
  print("End Main");
}
/*
Start main
Enter value
mohit
Exception Handled
End Main
*/