
// Write a dart prog, to determine if the user can cast a vote or not (a person with age above 18 can cast a vote)

void main(){

  var age1=18;
  if(age1 >= 18){

    print("You can cast a vote");
  }
  var age2=14;
  if(age2 < 18){
    print("You can't cast a vote");
  }
}