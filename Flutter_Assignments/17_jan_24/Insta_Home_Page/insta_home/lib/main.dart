import 'package:flutter/material.dart';
//import 'package:insta_home_page/assignment4.dart';

void main(){
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home:Assignment4(),
    );
  }
}

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});
  @override
  State<Assignment4>createState()=>_AssignmentState();
}

class _AssignmentState extends State<Assignment4>{
  bool _isPost1Liked=false;
  bool _isPost2Liked=false;
  bool _isPost3Liked=false;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        backgroundColor:Colors.white,
        title:const Text(
          "Instagram",
          style:TextStyle(
            fontStyle:FontStyle.italic,
            color:Colors.black,
            fontSize:30,
          ),
        ),
        actions:const [
          Icon(
            Icons.favorite_rounded,
            
          ),
          // Icon(
          //   Icons.message_outlined,
          //   color:Colors.black,
          // ),
        ],
      ),
      body:SingleChildScrollView(
        child:Column(
          children:[
            Column(
              mainAxisAlignment:MainAxisAlignment.start,
              crossAxisAlignment:CrossAxisAlignment.start,
              children:[
                Container(
              
                child:Image.network(
                  "https://www.hindustantimes.com/ht-img/img/2023/10/11/550x309/CRICKET-WORLDCUP-IND-AFG--57_1697044625493_1697044637037.JPG",
                  width:double.infinity,
                ),
              ),
              Row(
                children:[
                  IconButton(
                    onPressed:(){
                      setState(() {
                        _isPost1Liked=!_isPost1Liked;
                      });
                    },
                    icon:_isPost1Liked
                      ? const Icon(
                        Icons.favorite_outline_outlined,
                        color:Colors.red,
                      )
                      : const Icon(
                        Icons.favorite_outline_rounded,
                      ),
                    
                  ),
                  IconButton(
                    onPressed:(){},
                    icon:const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed:(){
                      
                    },
                    icon:const Icon(
                      Icons.send,
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed:(){},
                    icon:const Icon(
                      Icons.bookmark,
                    ),
                  ),
                ],
              ),
            
            ],
            ),
            Column(
              mainAxisAlignment:MainAxisAlignment.start,
              crossAxisAlignment:CrossAxisAlignment.start,
              children:[

                Image.network(
                  "https://cdn.britannica.com/52/252752-050-2E120356/Cricketer-Rohit-Sharma-2023.jpg",
                  width:double.infinity,
                ),
                Row(
                  children:[
                    IconButton(
                      onPressed:(){
                        setState(() {
                          _isPost2Liked=!_isPost2Liked;
                        });
                      },
                      icon:Icon(
                        _isPost2Liked
                          ? Icons.favorite_rounded
                        : Icons.favorite_outline_outlined,
                      
                        color:_isPost2Liked ? Colors.red:Colors.black,
                      ),                    
                    ),
                  
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.comment_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.send,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.bookmark_outline,
                      ),
                    ),

                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment:MainAxisAlignment.start,
              crossAxisAlignment:CrossAxisAlignment.start,
              children:[

                Image.network(
                  "https://im.rediff.com/cricket/2023/oct/19ro-1.png?w=670&h=900",
                  width:double.infinity,
                ),
                Row(
                  children:[
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.favorite_outline_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed:(){
                        setState(() {
                          _isPost3Liked=!_isPost3Liked;
                        });
                      },
                      icon:Icon(
                        _isPost3Liked
                          ? Icons.favorite_rounded
                        : Icons.favorite_outline_outlined,
                      
                        color:_isPost3Liked ? Colors.red:Colors.black,
                      ),                    
                    ),
                    IconButton(
                      onPressed: (){},
                      
                      icon:const Icon(
                        Icons.comment_outlined,
                      ),
                    ),
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.send,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed:(){},
                      icon:const Icon(
                        Icons.bookmark,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          
          ],  
        ),
      ),  
    );
    
  }
}

